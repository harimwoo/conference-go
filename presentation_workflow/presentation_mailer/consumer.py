import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
from django.core.mail import send_mail
import time


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


def process_approved_message(ch, method, properties, body):
    content = json.loads(body)
    name = content["presenter_name"]
    title = content["title"]
    email = content["presenter_email"]
    message = f"{name}, we're happy to tell you that your presentation {title} has been accepted"

    send_mail(
        "Your presentation has been accepted",
        message,
        "admin@conference.go",
        [email],
        fail_silently=False,
    )
    print("APPROVED MESSAGE SENT")


def process_rejectioned_message(ch, method, properties, body):
    content = json.loads(body)
    name = content["presenter_name"]
    title = content["title"]
    email = content["presenter_email"]
    message = f"{name}, we're sorry to tell you that your presentation {title} has been rejected"

    send_mail(
        "Your presentation has been rejected",
        message,
        "admin@conference.go",
        [email],
        fail_silently=False,
    )
    print("REJECTION MESSAGE SENT")


while True:
    try:
        parameters = pika.ConnectionParameters(host="rabbitmq")
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        channel.queue_declare(queue="presentation_approvals")
        channel.queue_declare(queue="presentation_rejections")
        channel.basic_consume(
            queue="presentation_approvals",
            on_message_callback=process_approved_message,
            auto_ack=True,
        )
        channel.basic_consume(
            queue="presentation_rejections",
            on_message_callback=process_rejectioned_message,
            auto_ack=True,
        )
        channel.start_consuming()
    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)
